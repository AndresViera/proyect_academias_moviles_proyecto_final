import AppRouter from "./routers/AppRouter";
import AuthProvider from "./auth/AuthProvider";
import {BrowserRouter} from 'react-router-dom';
import Layout from './components/layouts/Layout';
import {ToastContainer} from 'react-toastify';
function App() {
  return (
    <div className="App">
      
        <BrowserRouter>
          <AuthProvider>
          <Layout>
            <AppRouter />
          </Layout>
          </AuthProvider>
        </BrowserRouter>
      <ToastContainer />
    </div>
  );
}

export default App;
