import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import routes from '../helpers/routes';

export default function NotFoundPage() {
  return (
    <Container>
      <Row className='mt-5'>
        <Col md={{ span: 6, offset: 3 }}>
          <img style={{ width:'100%' }} src="/img/404-not-found.svg" alt="error-404" />
          <h2>Esta pagina no se encuentra</h2>
          <p>Vuelve al inicio <Link to={routes.home}>Inicio</Link> </p>
        </Col>
      </Row>
      
    </Container>
    
  )
}
