import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import routes from '../helpers/routes';

export default function HomePage() {
  return (
    <Container>
      <Row className='mt-5'>
        <Col xs={{ span: 12 }} md={{ span:6 }} className="mb-5">
          <h2>Bienvendi@ al Gestor de Tareas</h2>
          <p>Aqui podras gestoinar tus proyectos!</p>
          <p>Marca tus tareas como terminadas, agrega, elimina o actualiza</p>
          <div>
            <Link to={routes.login}>Ingresa</Link> o <Button as={Link} to={routes.register} >Crea una cuenta</Button>
          </div>
        </Col>

        <Col>
        <img className='img-fluid' src="/img/task-manager.svg" alt="task-manager" />
        <p>Gestiona tu tiempo, mejora tu proactividad!</p>
        </Col>
      </Row>
    </Container>
  )
}
