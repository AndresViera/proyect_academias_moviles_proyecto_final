import React, { useState } from 'react';
import {Modal, Alert, Button, Form} from 'react-bootstrap';
import useAuth from '../../../auth/useAuth';
import {toast} from 'react-toastify';

export default function ProfilePicModal({isOpen, close}) {

    const {logout} = useAuth();
    const [fileName, setFileName] = useState("Subir una imagen");
    const [selectedFile, setSelectedFile] = useState(null);

    const handleFileChange = (e)=>{
        const file = e.target.files[0];
        const SIZE_50MB = 50 * 1024 * 1024;
        const isValidSize = file.size < SIZE_50MB;
        //Expresión regular para saber si el nombre de un archivo termina en .jpg, jpeg, gif, png
        const isNameOfOneImageRegEx = /.(jpe?g|gif|png)$/i;
        const isValidType = isNameOfOneImageRegEx.test(file.name);
        if(!isValidSize) return toast.error('Imagen no valida por el tamaño maximo 50 MB');
        if(!isValidType) return toast.error('Solo puede subir imagenes!');
        setFileName(file.name);
        setSelectedFile(file);
        const reader = new FileReader();
        reader.onloadend = () =>{
            console.log(reader.result);
            setSelectedFile(reader.result);
        }
        reader.readAsDataURL(file);
        
        console.log(reader);
        console.log(e.target.files[0]);
    }

    const handleUpdateProfilePic = () =>{
        console.log(selectedFile);
        if(!selectedFile) return toast.warning('Debe seleccionar una imagen');

    }
    
  return (
      <Modal show={true} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>Cambiar foto de perfil</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="formFile" className="mb-3">
                <input type="file" data-browse='Subir' onChange={handleFileChange} accept='.jpg, .jpeg, .png, .gif'></input>
                <img width='150' height='150' className='img-fluid' src={selectedFile} alt="profile" />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
                <Button variant="secondary" onClick={close}>Cancelar</Button>
                <Button variant="primary" onClick={handleUpdateProfilePic}>Actualizar imagen</Button>
          </Modal.Footer>
      </Modal>
    
  )
}
