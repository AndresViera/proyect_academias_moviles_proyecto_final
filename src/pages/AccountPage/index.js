import React, { useState } from 'react';
import { Container, Row, Col, Button, Card } from 'react-bootstrap';
import useAuth from '../../auth/useAuth';
import useModal from '../../hooks/useModal';
import ChangePasswordModal from './components/ChangePasswordModal';
import DeleteModal from './components/DeleteModal';
import EditModal from './components/EditModal';
import ProfilePicModal from './components/ProfilePicModal';

export default function AccountPage() {

  const {user} = useAuth();

  // const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false);
  // const openDeleteModal =() => setIsOpenDeleteModal(true);
  // const closeDeleteModal =() => setIsOpenDeleteModal(false);

  // const [isOpenChangePasswordModal, setIsOpenChangePasswordModal] = useState(false);
  // const openChangePasswordModal =() => setIsOpenChangePasswordModal(true);
  // const closeChangePasswordModal =() => setIsOpenChangePasswordModal(false);

  const [isOpenDeleteModal, openDeleteModal, closeDeleteModal] = useModal(false);
  const [isOpenChangePasswordModal, openChangePasswordModal, closeChangePasswordModal] = useModal(false);
  const [isOpenEditModal, openEditModal, closeEditModal] = useModal(false);

  const [isOpenProfilePicModal, openProfilePicModal, closeProfilePicModal] = useModal(false);

  return (
    <>
    <Container>
      <Row>
        <Col xs={12} className="text-center">
          <img onClick={openProfilePicModal} style={{ width: '200px', height: '200px', cursos: 'pointer' }} src="/img/male_avatar.svg" alt="profile" />
        </Col>

        <Col className='mt-4'>
          <Card style={{ maxWidth: '360px' }} className="mx-auto p-4">
            <h4 className='text-center'>PROYECTO DE ACADEMIAS MOVILES MODULO I</h4>
            <p>Nombre: {user.name}</p>
            <p>Email: {user.email}</p>
            <p>Rol: {user.role}</p>
            <Button variant='warning' onClick={openEditModal}>Editar cuenta</Button>
            <Button variant='link' className='mt-1' onClick={openChangePasswordModal}>Cambiar contraseña</Button>
            <Button variant='link' className='mt-3 text-danger' onClick={openDeleteModal}>Eliminar cuenta</Button>
          </Card>
        </Col>
      </Row>
    </Container>
    <DeleteModal isOpen={isOpenDeleteModal} close={closeDeleteModal} />
    <ChangePasswordModal isOpen={isOpenChangePasswordModal} close={closeChangePasswordModal} />

    <EditModal user={user} isOpen={isOpenEditModal} close={closeEditModal} />

    <ProfilePicModal user={user} isOpen={isOpenProfilePicModal} close={closeProfilePicModal} />
    </>
    
    
  )
}
