import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import roles from '../helpers/roles';

const schema = yup.object().shape({
    name: yup.string('El nombre debe ser un texto').required('Este campo es requerido'),
    email: yup.string('El campo debe contener un texto').required('Este campo es requerido').email('Debe ingresar un email valido'),
    role: yup.string('El rol debe ser un texto').oneOf(Object.keys(roles), 'Escoja otro rol')
})

export default yupResolver(schema)