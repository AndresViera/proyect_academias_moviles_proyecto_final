import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';

const schema = yup.object().shape({
    password: yup.string('La password debe ser un texto').required('Debes ingresar una password').min(6, 'La password debe tener al menos 6 caracteres'),
})

export default yupResolver(schema)