import React, { useState } from 'react'

export default function useModal() {
    const [isOpen, setIsopen] = useState();
    const open =()=> setIsopen(true);
    const close =()=> setIsopen(false);
  return [isOpen, open, close]
}
